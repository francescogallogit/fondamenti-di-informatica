'''
Ex2.py

Calcolo della velocità media:

Per calcolare la velocità in km/h si deve dividere la distanza percorsa,
espressa in metri, per il tempo trascorso, in secondi, moltiplicare il
valore ottenuto per 3.6.

d = v * t => v = d/t * 3.6
'''
d = 0
v = 0
t = 0

d = 10
t = 63

d2metri = 10 * 1000
t2secondi = 63 * 60

v = d2metri / t2secondi * 3.6
print(v)

'''
Calcolo della cadenza o passo:

Data la velocità media espressa in km/h, si divide per 60.
Il risultato è in minuti e frazioni decimali di minuti.
Queste ultime vengono trasformate in secondi quando vengono moltiplicate per 60.

'''

passo = 60 / v # minuti e centesimi di minuto

#Dal risultato si sottrae il numero intero dei minuti
tmp = passo - 60 // v
tmp1 = tmp * 60
print("Passo: ", (60 // v),"minuti",tmp1,"secondi")

